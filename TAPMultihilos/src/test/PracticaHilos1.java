package test;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PracticaHilos1 {

    public static void main(String[] args) {
        
        TareaX hilos[] =  new TareaX[3];
        
        ThreadGroup tg = new ThreadGroup("Hilos");
        
        
        hilos[0] = new TareaX("Tarea 1 - ",10,2000);
        hilos[1] = new TareaX("Tarea 2 - ",5,4000);
        hilos[2] = new TareaX("Tarea 3 - ",20,500);
        
        
        hilos[0].start();
        hilos[1].start();
        hilos[2].start();
        
        
        tg.enumerate(hilos);
         
        tg.setDaemon(true);
        
        tg.interrupt();
        

    }

}

class TareaX extends Thread {

    int i = 0;
    String nombre;
    int seg;

    public TareaX(String nombre, int i, int seg) {

        this.i = i;
        this.nombre = nombre;
        this.seg = seg;

    }

    @Override
    public void run() {

        for (int x = 0; x < this.i; x++) {

            System.out.println(this.nombre + "- Hola mundo " + x);

            try {
                Thread.sleep(this.seg);
             } catch (InterruptedException ex) {
                Logger.getLogger(TareaX.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
